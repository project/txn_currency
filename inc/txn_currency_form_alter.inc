<?php
$currencies = array ();
$total = $form['screens']['cart']['cart']['totals'][1]['#value'];

foreach ($GLOBALS['user']->roles as $key => $val) {
  $currency = variable_get ('dvcurrency_currency_role_' . $key, '');
  
  foreach ($currency as $key => $val) {
    if ($key === $val) {
      if (!isset ($currencies[$key])) {
        $rating = variable_get ('dvcurrency_currency_'.$key, '0');
        $currencies[$key] = 
          strtoupper (preg_replace ('|([a-z])|', '$1.', $key)).
          '<div style="padding-left: 70px">$USD '. $total. ' = ' . strtoupper ($key) . ' ' . number_format ($total * $rating, 2) .'</div>';
      }
    }
  }
}

$form['currencyfieldset'] = array (
  '#type' => 'fieldset',
  'currency' => array (
    '#type'     => 'radios',
    '#title'    => t('Currency'),
    '#default_value' => 'aud',
		'#required' => true,
    '#description' => t('Please choose you favorite currency for the payment.'),
    '#options'  => $currencies
  )
);

$form['submit']['#weight'] = 10;